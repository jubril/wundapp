package com.test.wundertest;

import com.test.wundertest.interfaces.CarDetailsActivityContract;
import com.test.wundertest.model.CarDetails;
import com.test.wundertest.network.repository.CarRepository;
import com.test.wundertest.network.service.CarService;
import com.test.wundertest.network.service.CarServiceResponseCallback;
import com.test.wundertest.presenter.CarDetailsContractPresenterImpl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class CarDetailsContractPresenterImplTest {
    private CarDetailsContractPresenterImpl carDetailsContractPresenterImpl;

    @Mock
    private CarRepository carRepository;

    @Mock
    private CarDetailsActivityContract.View carDetailsView;

    @Mock
    private CarService carService;

    @Mock
    private CarServiceResponseCallback.CarDetailsSuccessResponse callback;

    @Mock
    private CarServiceResponseCallback.RentCarSuccessResponse rentCarSuccessResponse;

    @Mock
    private CarDetails carDetails;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        carDetailsContractPresenterImpl = Mockito.spy(new CarDetailsContractPresenterImpl(carDetailsView,carRepository));

    }

    @Test
    public void onErrorCallbackTest() {
        carDetailsContractPresenterImpl.onErrorCallback("Unable to get Data");
        Mockito.verify(carDetailsView,Mockito.times(1)).dismissLoading();
        Mockito.verify(carDetailsView,Mockito.times(1)).showMessage("Unable to get Data");
    }

    @Test
    public void onSuccessCallbackWithCarDetailsTest(){
        carDetailsContractPresenterImpl.onSuccessCallback(carDetails);
        Mockito.verify(carDetailsView,Mockito.times(1)).dismissLoading();
        Mockito.verify(carDetailsView,Mockito.times(1)).setUpCarDetails(carDetails);
        Mockito.verify(carDetailsView,Mockito.never()).showSnackBarMessage(null);
    }

    @Test
    public void onSuccessCallbackWithOutDataTest(){
        carDetails = null;
        carDetailsContractPresenterImpl.onSuccessCallback(carDetails);
        Mockito.verify(carDetailsView,Mockito.times(1)).dismissLoading();
        Mockito.verify(carDetailsView,Mockito.times(1)).showSnackBarMessage(null);
        Mockito.verify(carDetailsView,Mockito.never()).setUpCarDetails(carDetails);
    }


    @Test
    public void getCarDetailsTest(){
        carDetailsContractPresenterImpl.getCarDetails(1);
        Mockito.verify(carDetailsView,Mockito.times(1)).showLoading();
        Mockito.verify(carRepository,Mockito.times(1)).getCarDetails(1,carDetailsContractPresenterImpl);
    }

    @Test
    public void rentCarTest(){
        carDetailsContractPresenterImpl.rentCar(1);
        Mockito.verify(carDetailsView,Mockito.times(1)).showLoading();
        Mockito.verify(carRepository,Mockito.times(1)).rentCar(1,carDetailsContractPresenterImpl);
    }

}
