package com.test.wundertest;

import com.test.wundertest.interfaces.MapActivityContract;
import com.test.wundertest.model.Car;
import com.test.wundertest.network.repository.CarRepository;
import com.test.wundertest.presenter.MapActivityContractPresenterImpl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MapActivityContractPresenterImplTest {
    private MapActivityContractPresenterImpl mapActivityContractPresenter;

    @Mock
    private CarRepository carRepository;

    @Mock
    private MapActivityContract.View mapActivityView;

    @Mock
    private Car car;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mapActivityContractPresenter = Mockito.spy(new MapActivityContractPresenterImpl(mapActivityView,carRepository));

    }

    @Test
    public void onErrorCallbackTest() {
        mapActivityContractPresenter.onErrorCallback("Unable to get Data");
        Mockito.verify(mapActivityView,Mockito.times(1)).dismissLoading();
        Mockito.verify(mapActivityView,Mockito.times(1)).showMessage("Unable to get Data");
    }

    @Test
    public void onSuccessCallbackWithDataTest(){
        List<Car> carList = Arrays.asList(car,car,car);
        mapActivityContractPresenter.onSuccessCallback(carList);
        Mockito.verify(mapActivityView,Mockito.times(1)).dismissLoading();
        Mockito.verify(mapActivityView,Mockito.times(1)).setUpMapWithData(carList);
        Mockito.verify(mapActivityView,Mockito.never()).showSnackBarMessage(null);
    }

    @Test
    public void onSuccessCallbackWithOutDataTest(){
        List<Car> carList = new ArrayList<>();
        mapActivityContractPresenter.onSuccessCallback(carList);
        Mockito.verify(mapActivityView,Mockito.times(1)).dismissLoading();
        Mockito.verify(mapActivityView,Mockito.times(1)).showSnackBarMessage(null);
        Mockito.verify(mapActivityView,Mockito.never()).setUpMapWithData(carList);
    }

    @Test
    public void getListOfCarsTest(){
        mapActivityContractPresenter.getListOfCars();
        Mockito.verify(mapActivityView,Mockito.times(1)).showLoading();
        Mockito.verify(carRepository,Mockito.times(1)).getListOfCars(mapActivityContractPresenter);
    }

}
