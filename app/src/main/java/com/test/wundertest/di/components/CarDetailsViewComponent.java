package com.test.wundertest.di.components;

import com.test.wundertest.di.modules.ApplicationModule;
import com.test.wundertest.di.modules.CarDetailsModule;
import com.test.wundertest.di.modules.NetworkModule;
import com.test.wundertest.presenter.CarDetailsContractPresenterImpl;
import com.test.wundertest.view.CarDetailsActivity;

import dagger.Component;

@Component(modules = {CarDetailsModule.class, NetworkModule.class, ApplicationModule.class})
public interface CarDetailsViewComponent {

    CarDetailsActivity inject(CarDetailsActivity carDetailsActivity);

    CarDetailsContractPresenterImpl presenter();
}
