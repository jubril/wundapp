package com.test.wundertest.di.modules;

import com.test.wundertest.interfaces.MapActivityContract;
import com.test.wundertest.network.repository.CarRepository;
import com.test.wundertest.presenter.MapActivityContractPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class MapViewModule {
    private MapActivityContract.View mapView;


    public MapViewModule(MapActivityContract.View mapView) {
        this.mapView = mapView;
    }


    @Provides
    public MapActivityContractPresenterImpl providePresenter(CarRepository carRepository) {
        return new MapActivityContractPresenterImpl(mapView, carRepository);
    }


    @Provides
    MapActivityContract.View provideMapView() {
        return mapView;
    }


}
