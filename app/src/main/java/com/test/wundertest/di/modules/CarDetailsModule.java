package com.test.wundertest.di.modules;

import com.test.wundertest.interfaces.CarDetailsActivityContract;
import com.test.wundertest.network.repository.CarRepository;
import com.test.wundertest.presenter.CarDetailsContractPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class CarDetailsModule {
    private CarDetailsActivityContract.View carDetailsView;


    public CarDetailsModule(CarDetailsActivityContract.View carDetailsView) {
        this.carDetailsView = carDetailsView;
    }


    @Provides
    public CarDetailsContractPresenterImpl providePresenter(CarRepository carRepository) {
        return new CarDetailsContractPresenterImpl(carDetailsView, carRepository);
    }


    @Provides
    CarDetailsActivityContract.View provideCarDetailView() {
        return carDetailsView;
    }


}
