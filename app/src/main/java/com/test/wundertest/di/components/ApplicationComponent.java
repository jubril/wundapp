package com.test.wundertest.di.components;

import android.app.Activity;
import android.app.Application;

import com.test.wundertest.di.modules.ApplicationModule;

import dagger.Component;

@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(Activity target);

    Application getApplication();
}
