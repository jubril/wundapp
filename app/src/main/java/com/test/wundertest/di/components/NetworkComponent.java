package com.test.wundertest.di.components;


import com.test.wundertest.di.modules.CarDetailsModule;
import com.test.wundertest.di.modules.MapViewModule;
import com.test.wundertest.di.modules.NetworkModule;
import com.test.wundertest.network.repository.CarRepository;

import dagger.Component;

@Component(modules = {MapViewModule.class, CarDetailsModule.class, NetworkModule.class})
public interface NetworkComponent {

    CarRepository getCarRepository();

}
