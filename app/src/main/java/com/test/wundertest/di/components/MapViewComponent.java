package com.test.wundertest.di.components;

import com.test.wundertest.di.modules.ApplicationModule;
import com.test.wundertest.di.modules.MapViewModule;
import com.test.wundertest.di.modules.NetworkModule;
import com.test.wundertest.presenter.MapActivityContractPresenterImpl;
import com.test.wundertest.view.MapsActivity;

import dagger.Component;

@Component(modules = {MapViewModule.class, NetworkModule.class, ApplicationModule.class})
public interface MapViewComponent {

    MapsActivity inject(MapsActivity mapView);

    MapActivityContractPresenterImpl presenter();

}
