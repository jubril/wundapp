package com.test.wundertest.presenter;

import com.google.android.gms.maps.model.Marker;
import com.test.wundertest.interfaces.MapActivityContract;
import com.test.wundertest.model.Car;
import com.test.wundertest.network.repository.CarRepository;
import com.test.wundertest.network.service.CarServiceResponseCallback;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MapActivityContractPresenterImpl implements MapActivityContract.Presenter, CarServiceResponseCallback.CarListSuccessResponse {
    private MapActivityContract.View mapActivityView;
    private CarRepository carRepository;
    Integer selectedCar = 0;


    @Inject
    public MapActivityContractPresenterImpl(MapActivityContract.View mapActivityView, CarRepository carRepository) {
        this.mapActivityView = mapActivityView;
        this.carRepository = carRepository;
    }

    @Override
    public void getListOfCars() {
        mapActivityView.showLoading();
        carRepository.getListOfCars(this);
    }

    @Override
    public void onMarkerClicked(Marker selectedMarker, Integer carId) {
        if (selectedCar != 0) {
            mapActivityView.proceedToCarDetailsPage(carId);
        } else {
            selectedCar = carId;
            mapActivityView.displaySelectedMarker(selectedMarker);
        }
    }

    @Override
    public void refreshMap() {
        if (selectedCar != 0) {
            selectedCar = 0;
            mapActivityView.refreshMap();
        }
    }

    @Override
    public void onErrorCallback(String message) {
        mapActivityView.dismissLoading();
        mapActivityView.showMessage(message);
    }

    @Override
    public void onSuccessCallback(List<Car> carList) {
        mapActivityView.dismissLoading();
        if (carList.isEmpty()) {
            mapActivityView.showSnackBarMessage(null);
        } else {
            mapActivityView.setUpMapWithData(carList);
        }
    }
}
