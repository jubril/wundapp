package com.test.wundertest.presenter;

import com.test.wundertest.interfaces.CarDetailsActivityContract;
import com.test.wundertest.model.CarDetails;
import com.test.wundertest.model.CarRentResponse;
import com.test.wundertest.network.repository.CarRepository;
import com.test.wundertest.network.service.CarServiceResponseCallback;

import javax.inject.Inject;

public class CarDetailsContractPresenterImpl implements CarDetailsActivityContract.Presenter, CarServiceResponseCallback.CarDetailsSuccessResponse,
        CarServiceResponseCallback.RentCarSuccessResponse {

    private CarDetailsActivityContract.View carDetailsView;
    private CarRepository carRepository;

    @Inject
    public CarDetailsContractPresenterImpl(CarDetailsActivityContract.View carDetailsView, CarRepository carRepository) {
        this.carDetailsView = carDetailsView;
        this.carRepository = carRepository;
    }

    @Override
    public void getCarDetails(int carId) {
        carDetailsView.showLoading();
        carRepository.getCarDetails(carId, this);
    }

    @Override
    public void rentCar(int carId) {
        carDetailsView.showLoading();
        carRepository.rentCar(carId, this);

    }

    @Override
    public void onErrorCallback(String message) {
        carDetailsView.dismissLoading();
        carDetailsView.showMessage(message);
    }

    @Override
    public void onSuccessCallback(CarDetails carDetails) {
        carDetailsView.dismissLoading();
        carDetailsView.setUpCarDetails(carDetails);
    }

    @Override
    public void onSuccessCallback(CarRentResponse carRentResponse) {
        carDetailsView.dismissLoading();
        carDetailsView.showRentSuccess(carRentResponse);
    }
}
