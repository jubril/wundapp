package com.test.wundertest;

import android.app.Activity;
import android.app.Application;

import com.test.wundertest.di.components.ApplicationComponent;
import com.test.wundertest.di.components.DaggerApplicationComponent;
import com.test.wundertest.di.modules.ApplicationModule;

public class MyApplication extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
    }

    public static MyApplication get(Activity activity) {
        return (MyApplication) activity.getApplication();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
