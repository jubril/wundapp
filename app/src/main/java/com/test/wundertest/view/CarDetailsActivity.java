package com.test.wundertest.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;
import com.test.wundertest.R;
import com.test.wundertest.di.components.DaggerCarDetailsViewComponent;
import com.test.wundertest.di.modules.CarDetailsModule;
import com.test.wundertest.di.modules.NetworkModule;
import com.test.wundertest.interfaces.CarDetailsActivityContract;
import com.test.wundertest.model.CarDetails;
import com.test.wundertest.model.CarRentResponse;
import com.test.wundertest.presenter.CarDetailsContractPresenterImpl;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CarDetailsActivity extends AppCompatActivity implements CarDetailsActivityContract.View {
    @BindView(R.id.name_value)
    TextView nameTextView;
    @BindView(R.id.id_value)
    TextView iDTextView;
    @BindView(R.id.licence_plate_value)
    TextView licencePlateTextView;
    @BindView(R.id.fuel_level_value)
    TextView fuelLevelTextView;
    @BindView(R.id.pricing_time_value)
    TextView pricingTimeTextView;
    @BindView(R.id.pricing_parking_value)
    TextView pricingParkingTextView;
    @BindView(R.id.address_value)
    TextView addressTextView;
    @BindView(R.id.city_value)
    TextView cityTextView;
    @BindView(R.id.clean_value)
    TextView cleanTextView;
    @BindView(R.id.damaged_value)
    TextView damageTextView;
    @BindView(R.id.damaged_description_value)
    TextView damageDescriptionTextView;
    @BindView(R.id.zip_code_value)
    TextView zipCodeTextView;
    @BindView(R.id.hardware_activated_value)
    TextView hardwareActivatedTextView;
    @BindView(R.id.reservation_value)
    TextView reservationStateTextView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.constraint_layout)
    ConstraintLayout constraintLayout;
    @BindView(R.id.damage_description_row)
    TableRow damagedDescriptionTableRow;
    @BindView(R.id.imageView)
    ImageView imageView;
    @Inject
    CarDetailsContractPresenterImpl carDetailsContractPresenter;
    private int carId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_details);
        ButterKnife.bind(this);
        DaggerCarDetailsViewComponent.builder().networkModule(new NetworkModule())
                .carDetailsModule(new CarDetailsModule(this)).build().inject(this);
        carDetailsContractPresenter.getCarDetails(getIntent().getIntExtra(getString(R.string.car_id), 0));
    }

    @Override
    public void showLoading() {
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(android.view.View.VISIBLE);
    }

    @Override
    public void dismissLoading() {
        progressBar.setVisibility(android.view.View.GONE);
    }

    @Override
    public void showMessage(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getResources().getString(R.string.message));
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void showRentSuccessMessage(String message, String uriString) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getResources().getString(R.string.message));
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.view_location),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriString));
                        startActivity(intent);
                    }
                });
        alertDialog.show();
    }

    @Override
    public void showSnackBarMessage(String message) {
        if (TextUtils.isEmpty(message)) {
            message = getResources().getString(R.string.no_data_to_display);
        }
        Snackbar.make(constraintLayout, message,
                Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void setUpCarDetails(CarDetails carDetails) {
        Glide.with(this).load(carDetails.imageUrl).apply(RequestOptions.circleCropTransform()).into(imageView);
        nameTextView.setText(handleMissingValue(carDetails.carName));
        iDTextView.setText(String.valueOf(carDetails.carID));
        licencePlateTextView.setText(handleMissingValue(carDetails.licencePlate));
        fuelLevelTextView.setText(String.valueOf(carDetails.fuelLevel));
        pricingTimeTextView.setText(handleMissingValue(carDetails.pricingTime));
        pricingParkingTextView.setText(handleMissingValue(carDetails.pricingParking));
        addressTextView.setText(handleMissingValue(carDetails.address));
        cityTextView.setText(handleMissingValue(carDetails.city));
        zipCodeTextView.setText(handleMissingValue(carDetails.zipCode));
        hardwareActivatedTextView.setText(handleMissingValue(String.valueOf(carDetails.isActivatedByHardware)));
        reservationStateTextView.setText(handleMissingValue(String.valueOf(carDetails.reservationState)));
        cleanTextView.setText(handleMissingValue(String.valueOf(carDetails.isClean)));
        damageTextView.setText(handleMissingValue(String.valueOf(carDetails.isDamaged)));
        if (carDetails.isDamaged) {
            damagedDescriptionTableRow.setVisibility(View.VISIBLE);
            damageDescriptionTextView.setText(handleMissingValue(carDetails.damageDescription));
        }
        carId = carDetails.carID;

    }

    @Override
    public void showRentSuccess(CarRentResponse carRentResponse) {
        showRentSuccessMessage(getResources().getString(R.string.rent_success_message, carRentResponse.licencePlate),
                getResources().getString(R.string.open_map_string, carRentResponse.address));
    }

    public String handleMissingValue(String value) {
        return TextUtils.isEmpty(value) ? getResources().getString(R.string.unavailable) : value;
    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }


    @OnClick(R.id.button)
    public void onClickQuickRentButton() {
        carDetailsContractPresenter.rentCar(carId);
    }
}
