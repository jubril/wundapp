package com.test.wundertest.view;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.test.wundertest.R;
import com.test.wundertest.di.components.DaggerMapViewComponent;
import com.test.wundertest.di.modules.MapViewModule;
import com.test.wundertest.di.modules.NetworkModule;
import com.test.wundertest.interfaces.MapActivityContract;
import com.test.wundertest.model.Car;
import com.test.wundertest.presenter.MapActivityContractPresenterImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        MapActivityContract.View, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    private GoogleMap mMap;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    SupportMapFragment mapFragment;
    private HashMap<Marker, Integer> markerHashMap;
    private HashMap<Marker, Integer> temporaryMarkerHashMap;
    Marker tempMarker;
    @Inject
    MapActivityContractPresenterImpl mapActivityContractPresenter;
    private static final float ZOOM_VALUE = 15.8f;
    private static final int PERMISSIONS_REQUEST_LOCATION = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        DaggerMapViewComponent.builder().networkModule(new NetworkModule()).mapViewModule(new MapViewModule(this)).build().inject(this);
        mapActivityContractPresenter.getListOfCars();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                }
                return;
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_LOCATION);
        } else {
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void showLoading() {
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(android.view.View.VISIBLE);
    }

    @Override
    public void dismissLoading() {
        progressBar.setVisibility(android.view.View.GONE);
    }

    @Override
    public void showMessage(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getResources().getString(R.string.message));
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void showSnackBarMessage(String message) {
        if (message == null) {
            message = getResources().getString(R.string.no_data_to_display);
        }
        Snackbar.make(coordinatorLayout, message,
                Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void setUpMapWithData(List<Car> carList) {
        markerHashMap = new HashMap<>();
        for (Car car : carList) {
            LatLng latLng = new LatLng(car.latitude, car.longitude);
            markerHashMap.put(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.car))
                    .position(latLng)
                    .title(car.carName)), car.carID);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_VALUE));
        }
    }

    @Override
    public void displaySelectedMarker(Marker marker) {
        setMarkersVisible(false, marker);
    }

    @Override
    public void refreshMap() {
        setMarkersVisible(true, null);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        mapActivityContractPresenter.onMarkerClicked(marker, markerHashMap.get(marker));
        return false;
    }

    @Override
    public void proceedToCarDetailsPage(Integer carId) {
        Intent intent = new Intent(getBaseContext(), CarDetailsActivity.class);
        intent.putExtra(getResources().getString(R.string.car_id), carId);
        startActivity(intent);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mapActivityContractPresenter.refreshMap();
    }


    public void setMarkersVisible(boolean show, Marker marker) {
        for (Map.Entry<Marker, Integer> entry : markerHashMap.entrySet()) {
            if (entry.getValue() != markerHashMap.get(marker)) {
                entry.getKey().setVisible(show);
            }
        }
    }
}
