package com.test.wundertest.interfaces;

import com.google.android.gms.maps.model.Marker;
import com.test.wundertest.model.Car;

import java.util.List;

public interface MapActivityContract {

    interface Presenter {

        void getListOfCars();

        void onMarkerClicked(Marker selectedMarker, Integer carId);

        void refreshMap();

    }

    interface View {

        void showLoading();

        void dismissLoading();

        void showMessage(String message);

        void showSnackBarMessage(String message);

        void setUpMapWithData(List<Car> carList);

        void displaySelectedMarker(Marker marker);

        void refreshMap();

        void proceedToCarDetailsPage(Integer carId);
    }


}
