package com.test.wundertest.interfaces;

import com.test.wundertest.model.CarDetails;
import com.test.wundertest.model.CarRentResponse;

public interface CarDetailsActivityContract {
    interface Presenter {
        void getCarDetails(int carId);

        void rentCar(int carId);
    }

    interface View {

        void showLoading();

        void dismissLoading();

        void showMessage(String message);

        void showRentSuccessMessage(String message, String uriString);

        void showSnackBarMessage(String message);

        void setUpCarDetails(CarDetails carDetails);

        void showRentSuccess(CarRentResponse carRentResponse);

        boolean isNetworkConnected();
    }

}
