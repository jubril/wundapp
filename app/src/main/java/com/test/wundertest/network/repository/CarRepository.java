package com.test.wundertest.network.repository;

import com.test.wundertest.model.Car;
import com.test.wundertest.model.CarDetails;
import com.test.wundertest.model.CarRentRequest;
import com.test.wundertest.model.CarRentResponse;
import com.test.wundertest.network.service.CarService;
import com.test.wundertest.network.service.CarServiceResponseCallback;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarRepository {
    private CarService carService;

    @Inject
    public CarRepository(CarService carService) {
        this.carService = carService;
    }

    public void getListOfCars(final CarServiceResponseCallback.CarListSuccessResponse carListSuccessResponseCallback) {
        carService.getListOfCars().enqueue(new Callback<List<Car>>() {
            @Override
            public void onResponse(Call<List<Car>> call, Response<List<Car>> response) {
                if (response.isSuccessful()) {
                    carListSuccessResponseCallback.onSuccessCallback(response.body());
                } else {
                    carListSuccessResponseCallback.onErrorCallback(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Car>> call, Throwable t) {
                call.isCanceled();
                carListSuccessResponseCallback.onErrorCallback(t.getMessage());
            }
        });

    }

    public void getCarDetails(int carID, final CarServiceResponseCallback.CarDetailsSuccessResponse carDetailsSuccessResponseCallback) {
        carService.getCarDetails(carID).enqueue(new Callback<CarDetails>() {
            @Override
            public void onResponse(Call<CarDetails> call, Response<CarDetails> response) {
                if (response.isSuccessful()) {
                    carDetailsSuccessResponseCallback.onSuccessCallback(response.body());
                } else {
                    carDetailsSuccessResponseCallback.onErrorCallback(response.message());
                }
            }

            @Override
            public void onFailure(Call<CarDetails> call, Throwable t) {
                call.isCanceled();
                carDetailsSuccessResponseCallback.onErrorCallback(t.getMessage());
            }
        });
    }

    public void rentCar(int carID, final CarServiceResponseCallback.RentCarSuccessResponse rentCarSuccessResponseCallback) {
        carService.rentCar(new CarRentRequest(carID)).enqueue(new Callback<CarRentResponse>() {
            @Override
            public void onResponse(Call<CarRentResponse> call, Response<CarRentResponse> response) {
                if (response.isSuccessful()) {
                    rentCarSuccessResponseCallback.onSuccessCallback(response.body());
                } else {
                    rentCarSuccessResponseCallback.onErrorCallback(response.message());
                }
            }

            @Override
            public void onFailure(Call<CarRentResponse> call, Throwable t) {
                rentCarSuccessResponseCallback.onErrorCallback(t.getMessage());
            }
        });
    }
}
