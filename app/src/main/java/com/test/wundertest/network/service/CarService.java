package com.test.wundertest.network.service;

import com.test.wundertest.model.Car;
import com.test.wundertest.model.CarDetails;
import com.test.wundertest.model.CarRentRequest;
import com.test.wundertest.model.CarRentResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface CarService {

    @GET("/wunderfleet-recruiting-dev/cars.json")
    Call<List<Car>> getListOfCars();

    @GET("wunderfleet-recruiting-dev/cars/{carId}")
    Call<CarDetails> getCarDetails(@Path("carId") int carID);

    @Headers("Authorization: Bearer df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa")
    @POST("https://4i96gtjfia.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-mobile-dev-quick-rental")
    Call<CarRentResponse> rentCar(@Body CarRentRequest carRentRequest);
}
