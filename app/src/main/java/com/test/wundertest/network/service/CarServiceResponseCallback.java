package com.test.wundertest.network.service;

import com.test.wundertest.model.Car;
import com.test.wundertest.model.CarDetails;
import com.test.wundertest.model.CarRentResponse;

import java.util.List;

public interface CarServiceResponseCallback {
    public interface ErrorCallback {
        void onErrorCallback(String message);
    }

    public interface CarDetailsSuccessResponse extends ErrorCallback {
        void onSuccessCallback(CarDetails carDetails);
    }

    public interface CarListSuccessResponse extends ErrorCallback {
        void onSuccessCallback(List<Car> carList);
    }

    public interface RentCarSuccessResponse extends ErrorCallback {
        void onSuccessCallback(CarRentResponse carRentResponse);
    }
}
