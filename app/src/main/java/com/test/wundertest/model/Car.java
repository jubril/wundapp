package com.test.wundertest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Car extends BaseCar {
    @SerializedName("distance")
    @Expose
    public String Distance;
}
