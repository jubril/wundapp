package com.test.wundertest.model;

import com.google.gson.annotations.SerializedName;

public class CarRentResponse {
    @SerializedName("reservationId")
    public int reservationId;
    @SerializedName("carId")
    public int carId;
    @SerializedName("cost")
    public int cost;
    @SerializedName("drivenDistance")
    public int drivenDistance;
    @SerializedName("licencePlate")
    public String licencePlate;
    @SerializedName("startAddress")
    public String address;
    @SerializedName("userId")
    public int userId;
    @SerializedName("isParkModeEnabled")
    public boolean isParkModeEnabled;
    @SerializedName("damageDescription")
    public String damagedDescription;
    @SerializedName("fuelCardPin")
    public String fuelCardPin;
    @SerializedName("endTime")
    public Long endTime;
    @SerializedName("startTime")
    public Long startTime;
}
