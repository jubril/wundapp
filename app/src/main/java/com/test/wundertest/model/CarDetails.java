package com.test.wundertest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarDetails extends BaseCar {
    @SerializedName("hardwareId")
    @Expose
    public String hardwareId;
    @SerializedName("isActivatedByHardware")
    @Expose
    public boolean isActivatedByHardware;
    @SerializedName("damageDescription")
    @Expose
    public String damageDescription;
    @SerializedName("vehicleTypeImageUrl")
    @Expose
    public String imageUrl;
}
