package com.test.wundertest.model;

import com.google.gson.annotations.SerializedName;

public class CarRentRequest {
    @SerializedName("carId")
    public int carId;

    public CarRentRequest(int carId) {
        this.carId = carId;
    }
}
