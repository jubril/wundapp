package com.test.wundertest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseCar {
    @SerializedName("carId")
    @Expose
    public int carID;
    @SerializedName("title")
    @Expose
    public String carName;
    @SerializedName("lon")
    @Expose
    public double longitude;
    @SerializedName("lat")
    @Expose
    public double latitude;
    @SerializedName("licencePlate")
    @Expose
    public String licencePlate;
    @SerializedName("fuelLevel")
    @Expose
    public int fuelLevel;
    @SerializedName("vehicleStateId")
    @Expose
    public int vehicleStateId;
    @SerializedName("vehicleTypeId")
    @Expose
    public int vehicleTypeId;
    @SerializedName("pricingTime")
    @Expose
    public String pricingTime;
    @SerializedName("pricingParking")
    @Expose
    public String pricingParking;
    @SerializedName("reservationState")
    @Expose
    public int reservationState;
    @SerializedName("isClean")
    @Expose
    public boolean isClean;
    @SerializedName("isDamaged")
    @Expose
    public boolean isDamaged;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("zipCode")
    @Expose
    public String zipCode;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("locationId")
    @Expose
    public String locationId;
}
