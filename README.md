# **WunderMobility Interview App**

#### **Description**

-Upon opening the app, a user can see pins of cars on the map at their appropriate location

-Clicking the Pin once displays the name of the Car/Driver.

-Clicking the Map repopulates the pins.

-Clicking the Pin again takes the user to another page where details of the car is listed.

-On this page a user can simulate renting a car.

-After successfully renting a car. A user can navigate to the start address via google map or return to the Map Page.


#### **Requirements**

Java 8

Android Studio 3.4.*

Gradle 3.4.2


#### **App Architecture:**

MVP Pattern.

- Pros:

    Separation of Responsibilities
  
    Enhances Testability
  
    Enhances Modularity
    

#### **Tools Used:**

Java 8.

Android SDK.

Google Maps Android SDK.

Android X libraries.



#### **Third Party Libraries:**

ButterKnife : Used for View Binding.

Retrofit: Used for Network related Activity.

Glide: Used for Image Loading.

Mockito: Used for testing.

Retrofit Gson: Used for Json Deserialization.

Okhttp Logging: Used for Network Calls Logs.

Dagger: Used for handling Dependencies.



#### **Performance Optimization:**

Use of Static final for Constants to increase invocation time.

Minimal Object Creation to reduce garbage collection.



#### **Improvements**

Add more Tests

Investigate and handle leaks especially for orientation change

Handling Edge case scenarios e.g when there's no internet.

User Interface Design can be improved.

Handle Rate Limiting from API


